package com.petapi.demopetapi;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "Users")
public class User {

    @Id
    private String userId;
    private String userName;
    private String email;
    private String password;
    private List<Post> posts;

    protected User (){this.posts=new ArrayList<>();}

    public User (String userName, String email, String password,List<Post> posts) {
        this.userName=userName;
        this.email=email;
        this.password=password;
        this.posts=posts;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }


    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }



    public List<Post> getPosts() {
        return posts;
    }

}
