package com.petapi.demopetapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemopetapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemopetapiApplication.class, args);
    }

}
