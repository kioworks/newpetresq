package com.petapi.demopetapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/user")
public class UserController {

    private UsersRepo usersRepo;
    public UserController (UsersRepo usersRepo){
        this.usersRepo=usersRepo;
    }


    @GetMapping("/{userName}/{password}")
    public String login(@PathVariable("userName") String userName,@PathVariable ("password") String password){
       User loginUser=this.usersRepo.findByUserName(userName);

       while (loginUser!=null){
           System.out.println("user found");

           if (password.equals(loginUser.getPassword())){
               return "login sucess";
           }else{
               return "wrong password"+ loginUser.getUserName()+password;
           }
       }

       return "user does not exist";

    }

}