package com.petapi.demopetapi;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    private UsersRepo usersRepo;
    public PostController (UsersRepo usersRepo){
        this.usersRepo=usersRepo;
    }

    @CrossOrigin
    @GetMapping("/allposts")
    public List<Post> getAllPosts(){
        List<Post> allPosts = new ArrayList<>();
        List<User> allUsers = this.usersRepo.findAll();
        allUsers.forEach((user -> allPosts.addAll(user.getPosts())));
        return allPosts;
    }

}
