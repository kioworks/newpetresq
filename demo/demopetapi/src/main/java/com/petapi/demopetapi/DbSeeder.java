package com.petapi.demopetapi;

import javafx.geometry.Pos;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {

    private UsersRepo usersRepo;
    public DbSeeder (UsersRepo usersRepo){
        this.usersRepo=usersRepo;
    }

    @Override
    public void run(String... args) throws Exception {
        User admin = new User("admin",
                "admind@petresq.org",
                "admin",
                Arrays.asList(
                        new Post("Teddy",
                                "dog",
                                "Teddy is a 4 years old frenchbulldog...."),
                        new Post("Mini",
                                "cat",
                                "Mini is a cat...."),
                        new Post("Jack",
                                "dog",
                                "Jack is a dog...")
                )
        );
        User user1 = new User("user1",
                "user1@petresq.org",
                "user1",
                Arrays.asList(
                        new Post("Winston",
                                "dog",
                                "Winston is a dog..."),
                        new Post("Polly",
                                "bird",
                                "polly is a bird....")
                )
        );

        this.usersRepo.deleteAll();

        List<User> Users = Arrays.asList(admin,user1);
        this.usersRepo.insert(Users);



    }
}
