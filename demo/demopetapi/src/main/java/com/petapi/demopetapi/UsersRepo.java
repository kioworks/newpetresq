package com.petapi.demopetapi;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepo extends MongoRepository <User,String>{
    User findByUserName(String userName);
}
