package com.petapi.demopetapi;


public class Post {


    private String animalName;

    private String type;

    private String info;

    protected Post(){}

    public Post(String animalName, String type,String info) {
        this.animalName=animalName;
        this.type=type;
        this.info=info;
    }



    public String getAnimalName() {
        return animalName;
    }


    public String getType() {
        return type;
    }




    public String getInfo() {
        return info;
    }

}
